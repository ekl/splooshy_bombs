local grenadeDuration = 3
local gravity = 9.8

minetest.register_entity("splooshy_bombs_grenade:grenade_entity", {
  initial_properties = {
    visual = "sprite",
    textures = {"splooshy_bombs_grenade_pulled.png"}
  },
  _age = 0,
  _last_pos = false,
  explode = function(self, position)
    splooshy_bombs.boom(position, 4, {
      damage = 50,
      damage_radius = 8,
      vaporize = false
    })
  end,
  on_step = function(self, dtime, moveresult)
    self._age = self._age + dtime
    local obj = self.object
    if not obj then
      return
    end

    if self._age > grenadeDuration then
      self:explode(obj:get_pos())
      obj:remove()
      return
    end

    obj:add_velocity(vector.new(0, -gravity * dtime, 0))

    local current_pos = obj:get_pos()
    if not self._last_pos then -- Fallback to approximation if not available
      self._last_pos = current_pos:subtract(obj:get_velocity() * dtime)
    end
    -- Slight offset backwards in an attempt to reduce phasing
    local last_pos = self._last_pos:add(obj:get_velocity():normalize():multiply(-0.05))
    self._last_pos = current_pos

    -- do up to 3 bounces in a step to solve phasing bugs
    for _ = 1, 3 do
      local hit = Raycast(last_pos, current_pos, false, true):next()
      if not hit then
        break
      end
      minetest.chat_send_all(dump(hit))
      -- Do a bunch of math to bounce grenades
      local hitNormal = hit.intersection_normal
      local newVelocity = obj:get_velocity() / 2
      local newPos = current_pos
      for axis, value in pairs(hitNormal) do
        if value ~= 0 then
          -- Mirror the velocity
          newVelocity[axis] = -newVelocity[axis] / 2

          -- Mirror position across the hit surface to bring it back over
          newPos[axis] = hit.intersection_point[axis] * 2 - newPos[axis]
        end
      end
      current_pos = newPos
      obj:set_velocity(newVelocity)
      obj:move_to(newPos)
      self._last_pos = newPos
      last_pos = hit.intersection_point + hit.intersection_normal * .01
    end
  end
})

minetest.register_tool("splooshy_bombs_grenade:grenade", {
  description = "Grenade",
  visual = "wielditem",
  inventory_image = "splooshy_bombs_grenade.png",
  on_use = function(itemstack, user, pointed_thing)
    local entity = minetest.add_entity(user:get_pos():offset(0, user:get_properties().eye_height, 0),
        "splooshy_bombs_grenade:grenade_entity", nil)
    if entity then
      entity:set_velocity(user:get_velocity():add(user:get_look_dir():multiply(30)))
    end
    if not (minetest.check_player_privs(user, "creative")) then
      return ItemStack()
    end
  end
})

minetest.register_craft({
  output = "splooshy_bombs_grenade:grenade",
  recipe = {{"fire:flint_and_steel"}, {"tnt:tnt_stick"}}
})
