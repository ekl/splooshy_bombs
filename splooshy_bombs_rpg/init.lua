local rocketExpireSeconds = 10
local rocketPropulsionSeconds = 1.5
local rocketGravity = 9.8

minetest.register_entity("splooshy_bombs_rpg:rpg_entity", {
  initial_properties = {
    visual = "mesh",
    mesh = "splooshy_bombs_rpg_rocket.obj",
    textures = {"splooshy_bombs_rpg_rocket_ent.png"}
  },
  _age = 0,
  _last_pos = false,
  on_step = function(self, dtime, moveresult)
    self._age = self._age + dtime
    local obj = self.object
    if not obj then
      return
    end

    if self._age > rocketExpireSeconds then
      obj:remove()
      return
    end

    if self._age < rocketPropulsionSeconds then
      obj:add_velocity(vector.new(0, 0, dtime * 100 * (math.min(self._age ^ 2, 1))):rotate(obj:get_rotation()))
    end

    obj:add_velocity(vector.new(0, -rocketGravity * dtime, 0))

    local current_pos = obj:get_pos()
    if not self._last_pos then -- Fallback to approximation if not available
      self._last_pos = current_pos:subtract(obj:get_velocity() * dtime)
    end
    -- Slight offset backwards in an attempt to reduce phasing
    local last_pos = self._last_pos:add(obj:get_velocity():normalize():multiply(0.1))
    self._last_pos = current_pos

    local hit = Raycast(last_pos, current_pos, false, true):next()
    if hit then
      splooshy_bombs.boom(hit.under or current_pos, 5, {
        damage = 50,
        damage_radius = 7,
        vaporize = false,
        velocity = obj:get_velocity():multiply(0.05):offset(0, 20, 0)
      }, nil)
      obj:remove()
    end
  end
})

minetest.register_tool("splooshy_bombs_rpg:rocket_tube_loaded", {
  description = "Rocket launcher",
  visual = "wielditem",
  inventory_image = "splooshy_bombs_rpg_loaded.png",
  on_use = function(itemstack, user, pointed_thing)
    local entity = minetest.add_entity(user:get_pos():offset(0, user:get_properties().eye_height, 0),
        "splooshy_bombs_rpg:rpg_entity", nil)
    if entity then
      entity:set_velocity(user:get_velocity():add(user:get_look_dir():multiply(20)))
      entity:set_rotation(user:get_look_dir():dir_to_rotation())
    end
    if not (minetest.check_player_privs(user, "creative")) then
      return ItemStack("splooshy_bombs_rpg:rocket_tube")
    end
  end
})

minetest.register_craftitem("splooshy_bombs_rpg:rocket_tube", {
  description = "Rocket launcher",
  visual = "wielditem",
  inventory_image = "splooshy_bombs_rpg_unloaded.png"
})

minetest.register_craftitem("splooshy_bombs_rpg:rocket", {
  description = "RPG Rocket",
  visual = "wielditem",
  inventory_image = "splooshy_bombs_rpg_rocket.png"
})

minetest.register_craft({
  type = "shapeless",
  output = "splooshy_bombs_rpg:rocket_tube_loaded",
  recipe = {"splooshy_bombs_rpg:rocket_tube", "splooshy_bombs_rpg:rocket"}
})

-- Crafts
minetest.register_craft({
  output = "splooshy_bombs_rpg:rocket_tube",
  recipe = {{"default:steel_ingot", "default:steel_ingot", "default:steel_ingot"},
            {"default:steel_ingot", "default:steel_ingot", "default:steel_ingot"},
            {"default:stick", "", "fire:flint_and_steel"}}
})

minetest.register_craft({
  output = "splooshy_bombs_rpg:rocket",
  recipe = {{"tnt:tnt_stick", "", ""}, {"", "default:steel_ingot", ""}, {"", "", "default:coal_lump"}}
})
