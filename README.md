# Splooshy Bombs

Weapons using the Splooshy Bombs API.

Current weapons:
Grenade (explodes on contact)
RPG

Note: The RPG has some intentionally funky ballistics. Aim high to account for gravity and the burn characteristics.